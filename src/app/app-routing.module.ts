import { CharactersDetailPageComponent } from './pages/characters-page/pages/characters-detail-page/characters-detail-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import { LocationsPageComponent } from './pages/locations-page/locations-page.component';
import { FavouritesPageComponent } from './pages/favourites-page/favourites-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'characters', component: CharactersPageComponent},
  {path: 'characters/:idCharacters', component: CharactersDetailPageComponent},
  {path: 'locations', component: LocationsPageComponent},
  {path: 'favourites', component: FavouritesPageComponent},
  {path: 'contact', component: ContactPageComponent}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
