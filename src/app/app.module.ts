import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { NavComponent } from './core/components/nav/nav.component';
import { CharactersPageComponent } from './pages/characters-page/characters-page.component';
import {​​ HttpClientModule }​​ from '@angular/common/http';
import { GalleryComponent } from './shared/components/gallery/gallery.component';
import { LocationsPageComponent } from './pages/locations-page/locations-page.component';
import { FavouritesPageComponent } from './pages/favourites-page/favourites-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CharactersDetailPageComponent } from './pages/characters-page/pages/characters-detail-page/characters-detail-page.component';
import { MultiplyPipe } from './shared/pipes/multiply.pipe';
import { PriorityNamePipe } from './shared/pipes/priority-name.pipe'
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavComponent,
    CharactersPageComponent,
    GalleryComponent,
    LocationsPageComponent,
    FavouritesPageComponent,
    ContactPageComponent,
    CharactersDetailPageComponent,
    MultiplyPipe,
    PriorityNamePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
