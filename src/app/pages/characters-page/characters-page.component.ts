import { CharactersService } from './../../shared/services/characters.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-characters-page',
  templateUrl: './characters-page.component.html',
  styleUrls: ['./characters-page.component.scss']
})
export class CharactersPageComponent implements OnInit {

  characters: any;
  character: any;
  actualPage = 1;

  constructor(private charactersService: CharactersService ) {}


  ngOnInit(): void {
    this.charactersService.getCharacters().subscribe((data: any) => {
      this.characters = data.results;
    })
  }

  findCharacter(character: any, actualPage: any){
    this.actualPage = actualPage
    this.charactersService.getCharacters(character, actualPage).subscribe((data: any) => {
      this.characters = data.results;
    })
  }

}

   