import { ActivatedRoute } from '@angular/router';
import { CharactersService } from './../../../../shared/services/characters.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-characters-detail-page',
  templateUrl: './characters-detail-page.component.html',
  styleUrls: ['./characters-detail-page.component.scss']
})
export class CharactersDetailPageComponent implements OnInit {

  findCharacter: any;
  character: any;

  constructor(private charactersService: CharactersService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.findCharacter = params.get('idCharacters');

        this.charactersService.getCharactersDetail(this.findCharacter).subscribe((data: any) => {
          console.log(data)
          this.character = data;
        })
    });
  }

}
