import { FavouritesService } from './../../shared/local/favourites.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favourites-page',
  templateUrl: './favourites-page.component.html',
  styleUrls: ['./favourites-page.component.scss']
})
export class FavouritesPageComponent implements OnInit {

  favourite: any;

  constructor(private favouritesService: FavouritesService) { }

  ngOnInit(): void {
    this.favourite = this.favouritesService.getFavourites();
  }


}
