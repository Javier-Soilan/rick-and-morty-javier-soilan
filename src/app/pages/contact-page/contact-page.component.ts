import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {

  submitted = false;

  contactForm:any;

  constructor(private formBuilder: FormBuilder) {
    this.contactForm= this.formBuilder.group({

      name:['', [Validators.required, Validators.minLength(4)]],
      email:['', [Validators.required, Validators.email]],
      message:['', [Validators.required, Validators.minLength(10)]],
      checkbox:[false, [Validators.requiredTrue]]
    })
  }

  ngOnInit(): void {
  }


    submitForm(){
      this.submitted = true;
      console.log(this.contactForm.controls.name)
      console.log(this.contactForm)
      console.log(this.contactForm.value)
    }
}
