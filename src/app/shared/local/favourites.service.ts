import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavouritesService {

  private favourites = [];

  constructor() { }

  addFavourite(newFavourite){
    this.favourites.push(newFavourite);
  }

  getFavourites(){
    return this.favourites;
  }
}
