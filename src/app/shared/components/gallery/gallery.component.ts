import { FavouritesService } from './../../local/favourites.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() list: any;

  constructor(private favouritesService: FavouritesService) {}

  ngOnInit(): void {
  }

  addFavouriteItem(favItem: any){
    this.favouritesService.addFavourite(favItem);
  }
}
  