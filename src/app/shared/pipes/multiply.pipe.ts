import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiply'
})
export class MultiplyPipe implements PipeTransform {

  transform(value: number, arg1, arg2): number {
    return value * arg1 * arg2;
  }

}
/* number es el tipo que queremos que sea nuestro value */

/* {{value | multiply: arg1, arg2}} esta seria la estructura que pondiramos en el html, asignandole valores. Ej: {{40 | multiply: 2, 25}}*/